# install-octocli

*The code in this repository is still in progress*

The `install-octocli` repository contains code for installing the Octopus Deploy CLI in languages including:
1. Go
2. Python
3. JavaScript
4. PowerShell

The directories for each language contain a `.gitlab-ci.yml` GitLab template that you can use to install Octopus Deploy in a GitLab environment.

## Examples