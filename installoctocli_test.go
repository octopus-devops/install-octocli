package main

import (
	"os"
	"os/exec"
	"runtime"
	"testing"

	"github.com/go-ping/ping"
)

func TestInstalloctocli(t *testing.T) {
	if runtime.GOOS == "windows" {
		cmd1 := exec.Command("$PSVersionTable.PSVersion")
		cmd1.Stdout = os.Stdout

		err1 := cmd1.Run()

		if err1 != nil {
			t.Errorf("PowerShell not installed. Please ensure PowerShell is installed to run the installation of Octopus Deploy CLI")
		} else {
			cmd2 := exec.Command("choco")
			cmd2.Stdout = os.Stdout

			err2 := cmd2.Run()

			if err2 != nil {
				t.Errorf("Chocolatey is not installed. Please ensure Chocolatey package manager is installed to run the installation of Octopus Deploy CLI")
			}
		}
	}

	if runtime.GOOS == "darwin" {
		cmd1 := exec.Command("brew", "--version")
		cmd1.Stdout = os.Stdout

		err1 := cmd1.Run()

		if err1 != nil {

			t.Errorf("Homebrew package manager is not installed. Please ensure Homebrew is installed to run the installation of Octopus Deploy CLI")
		}
	}
}

func TestOperatingSystem(t *testing.T) {
	if runtime.GOOS == "Windows" {
		t.Log(nil)
	} else if runtime.GOOS == "darwin" {
		t.Log(nil)
	} else if runtime.GOOS == "linux" {
		t.Log(nil)
	} else {
		t.Errorf("An unsupported operating system is being used. Must be Windows, Linux, or MacOS")
	}
}

func TestOutboundNetworkConnecticity(t *testing.T) {
	// Installing the Octopus CLI requires going out to the internet
	// This test will test outbound connectivity

	ipPing, err1 := ping.NewPinger("8.8.8.8")
	addressPing, err2 := ping.NewPinger("www.google.com")

	if err1 != nil {
		t.Log(ipPing)
		t.Errorf("Network Outbound Connectibity Unreachable")
	} else if err2 != nil {
		t.Log(addressPing)
		t.Errorf("Network Outbound Connectibity Unreachable")
	}
}
